package com.kodeashians.hci.screen;

import com.kodeashians.hci.BlocksHCI;
import com.kodeashians.hci.Button;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import java.util.List;
import java.util.ArrayList;

import com.DramaCow.maths.*;

public class BlocksScreen implements Screen {

	private final BlocksHCI game;
	private OrthographicCamera cam;

	private Vector3 touchPoint = new Vector3();
	private AABB blockTouched = null;

	private AABB grid;
	private AABB pin = new AABB(0.0f, 0.0f, 0.125f, 0.125f);
	private List<AABB> blocks = new ArrayList<AABB>();

	private Button btnReset;

	private final float vp = 6.9375f;

	public BlocksScreen(final BlocksHCI game) {
		this.game = game;

		float w = game.getScreenWidth(), h = game.getScreenHeight();
		cam = new OrthographicCamera(vp * ((float) w/h), vp);
		cam.position.set(cam.viewportWidth / 2.0f, cam.viewportHeight / 2.0f, 0.0f);
		cam.update();

		grid = new AABB(8.0f, vp / 2.0f, 3.0f, 3.0f);

		float inity = vp / 2.0f - 2.65f;
		for (int i = 0; i < 24; i++) {
			blocks.add(
				new AABB(0.85f + (i % 4)*1.0625f, inity + (i / 4)*1.0625f, 0.5f, 0.5f)
			);
		}

		btnReset = new Button(new AABB(11.5f, vp-0.225f, 0.4f, 0.125f)) {
			@Override
			public void onClick() {
				game.setScreen(new BlocksScreen(game));
			}
		};
	}	

	@Override
	public void show() {

	}

	@Override
	public void update(float delta) {
		// Get mouse/touch point
		cam.unproject(touchPoint.set(game.getInputX(), game.getInputY(), 0));

		if (game.isInputTouched() && blockTouched == null) {
			for (AABB block : blocks) {
				if (block.contains(touchPoint.x, touchPoint.y)) {				
					blockTouched = block;
					break;
				}
			}
		}
		else if (!game.isInputTouched()) {
			if (grid.contains(touchPoint.x, touchPoint.y) && blockTouched != null) {
				// 2.0 factor used to round to nearest 0.5				
				float x = 2.0f * (touchPoint.x - grid.position.x % 1.0f);
				float y = 2.0f * (touchPoint.y - grid.position.y % 1.0f);

				x = (float)Math.round(x) / 2.0f + (grid.position.x % 1.0f);
				y = (float)Math.round(y) / 2.0f + (grid.position.y % 1.0f);

				blockTouched.position.x = x;
				blockTouched.position.y = y;
			}

			blockTouched = null;
		}

		//System.out.println(blockTouched);
		
		if (blockTouched != null) {
			blockTouched.position.x = touchPoint.x;
			blockTouched.position.y = touchPoint.y;
		}

		btnReset.update(touchPoint.x, touchPoint.y, game.isInputTouched());
	}

	public void	draw () {
		cam.update();

		game.shape.setProjectionMatrix(cam.combined);
		game.shape.begin(ShapeType.Filled);
			// Last parameter of circle refers to number of segments used to represent the circle 
			//	- the higher the count, the smoother the circle

			game.shape.setColor(0.0f, 0.5f, 0.0f, 1.0f);
			game.shape.rect(grid.position.x - grid.halfExtents.x, grid.position.y - grid.halfExtents.y,
				2.0f * grid.halfExtents.x, 2.0f * grid.halfExtents.y);

			game.shape.setColor(0.0f, 0.5625f, 0.0f, 1.0f);
			for (float j = 0.125f; j < 2.0f * grid.halfExtents.y; j += 0.5f) {
				for (float i = 0.125f; i < 2.0f * grid.halfExtents.x; i += 0.5f) {
					game.shape.rect(grid.position.x - grid.halfExtents.x + i, grid.position.y - grid.halfExtents.y + j,
						2.0f * pin.halfExtents.x, 2.0f * pin.halfExtents.y);
				}
			}			

			for (AABB block : blocks) {
				game.shape.setColor(0.5f, 0.0f, 0.0f, 1.0f);
				game.shape.rect(block.position.x - block.halfExtents.x, block.position.y - block.halfExtents.y, 
					2.0f * block.halfExtents.x, 2.0f * block.halfExtents.y);

				game.shape.setColor(0.5625f, 0.0f, 0.0f, 1.0f);
				for (float j = 0.125f; j < 1.0f; j += 0.5f) {
					for (float i = 0.125f; i < 1.0f; i += 0.5f) {
						game.shape.rect(block.position.x - block.halfExtents.x + i, block.position.y - block.halfExtents.y + j,
							2.0f * pin.halfExtents.x, 2.0f * pin.halfExtents.y);
					}
				}			
			}

			game.shape.setColor(0.0f, 0.0f, 1.0f, 1.0f);
			game.shape.rect(btnReset.box.position.x - btnReset.box.halfExtents.x, btnReset.box.position.y - btnReset.box.halfExtents.y, 
					2.0f * btnReset.box.halfExtents.x, 2.0f * btnReset.box.halfExtents.y);
		game.shape.end();
	}

	@Override
	public void resize(int w, int h) {
		cam.viewportWidth = vp * ((float) w/h);
		cam.viewportHeight = vp;
		cam.position.set(cam.viewportWidth / 2.0f, cam.viewportHeight / 2.0f, 0.0f);
		cam.update();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {

	}
}
