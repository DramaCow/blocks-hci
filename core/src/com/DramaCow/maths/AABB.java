// Sam Coward - 2015

package com.DramaCow.maths;

// Axis Aligned Bounding Box (used for collision detection/reaction)
public class AABB {
	
	// Offset (by center)
	public Vector2D position;

	// Dimensions
	public Vector2D halfExtents; // half of dimensions

	public AABB(float x, float y, float halfWidth, float halfHeight) {
		this.position = new Vector2D(x, y);
		this.halfExtents = new Vector2D(halfWidth, halfHeight);
	}

	public AABB(Vector2D position, Vector2D halfExtents) {
		this.position = position;
		this.halfExtents = halfExtents;
	}

	public AABB(AABB box) {
		this.position = new Vector2D(box.position);
		this.halfExtents = new Vector2D(box.halfExtents);
	}

	public boolean contains(float x, float y) {
		return Math.abs(position.x - x) <= Math.abs(halfExtents.x) &&
			Math.abs(position.y - y) <= Math.abs(halfExtents.y); 
	}

	public boolean contains(Vector2D point) {
		return contains(point.x, point.y);
	}

	@Override
	public String toString() {
		return "position=" + position.toString() + " " + "halfExtents=" + halfExtents.toString();
	}
}
