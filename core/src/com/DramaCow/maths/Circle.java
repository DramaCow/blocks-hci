// Sam Coward - 2015

package com.DramaCow.maths;

public class Circle {
	// Offset (by center)
	public Vector2D position;

	// Dimensions
	public float radius;

	public Circle(float x, float y, float r) {
		this.position = new Vector2D(x, y);
		this.radius = r;
	}

	public Circle(Vector2D position, float r) {
		this.position = position;
		this.radius = r;
	}

	public boolean contains(float x, float y) {
		return (position.x - x) * (position.x - x) 
				+ (position.y - y) * (position.y - y) 
				<= radius * radius;
	}

	public boolean contains(Vector2D point) {
		return contains(point.x, point.y);
	}
}
