package com.kodeashians.hci;

import com.kodeashians.hci.screen.Screen;
import com.kodeashians.hci.screen.BlocksScreen;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

public class BlocksHCI extends ApplicationAdapter {

	public SpriteBatch batch;
	public ShapeRenderer shape;

	private Screen screen;

	@Override
	public void create() {
		batch = new SpriteBatch();
		shape = new ShapeRenderer();

		setColour(0.0f, 0.0f, 0.0f, 1.0f);
		setScreen(new BlocksScreen(this));
	}

	@Override
	public void render() {
		screen.update(Gdx.graphics.getDeltaTime());
		clear();
		screen.draw();
	}

	@Override	
	public void pause() {
		screen.pause();
	}

	@Override	
	public void resume() {
		screen.resume();
	}

	@Override
	public void resize(int w, int h) {
		screen.resize(w, h);
	}

	@Override	
	public void dispose() {
		screen.dispose();
	}

	public void setScreen(Screen s) {
		if (screen != null) {
            screen.hide(); 													// dispose of assets
        }
        screen = s;
        screen.show(); 														// load assets (MAKE SURE TO NOT INITIALISE LOGIC HERE)
        screen.resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
	}

	public void setColour(float r, float g, float b, float a) {
		Gdx.gl.glClearColor(r, g, b, a);
	}

	// Only needs to be called once per frame
	public void clear() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
	}

	public int getScreenWidth() {
		return Gdx.graphics.getWidth();
	}

	public int getScreenHeight() {
		return Gdx.graphics.getHeight();
	}

	public float getInputX() {
		return Gdx.input.getX();
	}

	public float getInputY() {
		return Gdx.input.getY();
	}	

	public boolean isInputTouched() {
		return Gdx.input.isTouched();
	}
}
