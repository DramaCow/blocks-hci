package com.kodeashians.hci;

import com.DramaCow.maths.AABB;

public class Button {

	public static enum ButtonState {
		IDLE, 
		HOVER, 
		CLICK
	}

	public AABB box;

	private ButtonState state = ButtonState.IDLE;
	private boolean hasClicked = false; // Used to prevent repeated onClick calls when button is held

	public Button(AABB box) {
		this.box = box;
	}

	//To be called in the update function of the screen class
	public final void update(float x, float y, boolean down) {
		if (hasClicked && !down) hasClicked = false;	

		//If the mouse/touchscreen is on the button
		if (box.contains(x, y)) {
			//Check if touched or hovered
			if (down) {
				state = ButtonState.CLICK;
				if (!hasClicked) {
					hasClicked = true;
					onClick();
				}
			} 
			else {
				state = ButtonState.HOVER;
			}
		} 
		else {
			state = ButtonState.IDLE;
		}
	}

	// To be overidden
	public void onClick() {}
}
